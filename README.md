# IR-code for Loriot Sky AirConditioner

IR-codes from Loriot Sky air conditioning unit with YKR-T/011E remote control. May be compatible with Electra and AUX brands.

Uses [NEC ir-signal coding](https://techdocs.altium.com/display/FPGA/NEC+Infrared+Transmission+Protocol) at a carrier frequency of 38kHz.

Introduction of 9000µs and 4500µs is present. Then goes the command without address and inverse repetition.

The ending 650µs pulse is also present.

The length of the message is 104 bits (13 bytes).

## Message structure

|   Bits   |    Function        | Descrition |
|----------|--------------------|------------|
| 0  -  7  | unchanged part UC0 | **11000011** - unchanged  |
| 8  - 10  | Vertical swing     | **111** - off, **000** - on; +88-90 bits are changed sometimes - needs additional check|
| 11 - 15  | Temperature        | set temperature -8°C in reverse order of bits (32-8=24 -> 0b11000 -> **00011**) |
| 16 - 20  | UC1                | **00000** - unchanged(?)    |
| 21 - 23  | Horizontal swing   | **111** - off, **000** - on; +88-90 bits are changed sometimes - needs additional check|
| 24 - 31  | UC2                | **00000000** - unchanged(?) |
| 32 - 36  | Timer hours        | Timer hours in reverse order of bits (24 -> 0b11000 -> **00011**) |
| 37 - 39  | Fan Speed          | **101** - Auto, **100** - High (see bit 46), **010** - Medium, **110** - Low.  |
| 40 - 45  | Timer minutes      | Timer minutes in reverse order of bits (24 -> 0b11000 -> **000110**) |
|   46     | Turbo speed bit    | **1** - on, **0** - off when speed is High (see bits 37-39). |
| 47 - 52  | UC3                | **000000** - unchanged(?) |
| 53 - 55  | Mode               | **000** - Auto, **100** - Cool, **010** - Dry, **001** - Heat (see bit 76), **011** - Fan |
| 56 - 75  | UC4                | **00000000** - unchanged(???) Bit 73 may be connected with timer function |
|   76     | Heat mode bit      | **1** - on, **0** - off (see bits 53-55). Is **0** when AC is off. |
|   77     | On-Off bit         | **1** - on, **0** - off |
| 78 - 79  | Timer              | **00** - off, **01** - OFF-timer, **10** - ON-timer |
| 80 - 87  | UC5                | **00000000** - unchanged(?) |
| 88 - 91  | ? | Bits with strange behaviour. Changed with swing-change, but not constant. |
|    92    | Display            | **1** - Toggle display on/off, **0** - do nothing |
| 93 - 95  | ? | **010** - Bits with strange behaviour. Maybe unchanged. |
| 96 - 103 |    CRC             | Read the next part |

### CRC calculation

* Sum first 12 bytes with inversed bit order.
* Drop overflow of 8 bits.
* Inverse bit order of the sum you've got.

## Example Message

Message of switched ON, cooling, 26C, Vertical and Horizontal swing at low speed.

This message was got with the help of Arduino IR receiver.

The default buffer length is too small to get this message. To get one, change a line of IRremoteint.h in Arduino libraries to:

```C
#define RAWBUF  301
```

### Raw message

```c
9000 -4400
650 -1600 700 -1550 700 -450  650 -450 650 -500  650 -450  700 -1600 650 -1600
650 -450  650 -500  650 -500  650 -450 650 -1600 650 -500  650 -450  700 -1550
700 -450  650 -500  650 -450  650 -500 650 -500  650 -450  650 -500  650 -500
650 -450  650 -500  650 -450  650 -500 650 -500  650 -450  650 -500  650 -500
600 -500  700 -450  650 -450  650 -500 650 -450  700 -1600 650 -1600 650 -450
650 -500  650 -500  600 -500  650 -500 650 -450  650 -500  650 -500  650 -450
700 -450  650 -500  650 -450  650 -500 650 -450  650 -1600 700 -450  650 -500
650 -450  650 -500  650 -500  600 -500 700 -450  650 -450  650 -500  650 -500
650 -450  650 -500  650 -450  650 -500 700 -450  650 -450  650 -500  650 -500
650 -450  650 -500  650 -450  650 -500 650 -500  650 -1600 650 -450  650 -500
650 -500  650 -450  650 -500  650 -450 650 -500  650 -500  650 -450  650 -500
650 -500  650 -450  650 -1600 650 -500 650 -500  650 -450  650 -1600 650 -500
650 -1600 650 -1600 650 -1600 650 -450 700 -1550 650 -1600 700 -450  650 -500
650
```

### Message decoding

Intro bits are left out.

```text
11000011000010010000000000000000000001100000000000000100000000000000000000000100000000000010001011101100
--UC0--|-V-26°C-|UC1|-H-|-UC2--|-----Low---------|U3|COL|-------UC4-------|--N--|-UC5--|0010-010|++crc++
01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123
0         1         2         3         4         5         6         7         8         9         0
```
